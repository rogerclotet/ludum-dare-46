# Earth Last Defender

This is the game I made for [Ludum Dare 46](https://ldjam.com/events/ludum-dare/46): https://ldjam.com/events/ludum-dare/46/earth-last-defender

## About the game

![Earth Last Defender](/Resources/title.png)

Earth Last Defender is a game about keeping alive our beloved planet.

You are in charge of the last spaceship with a simple mission: protect Earth from the alien forces threatening our planet.

You start with a simple laser weapon, but scientists on Earth will aid you with some improvements, with extra fire rate or more weapons.

The alien forces are getting stronger, so it's just a matter of time that we lose all hope, but try to keep the planet alive for as long as you can!

**You can play the game [here](https://eld.clotet.dev/old)!**

### :video_game: Controls

The ship is controlled with WASD keys and mouse to aim and fire.

### :camera: Screenshots

![Screenshot](/Resources/screenshot1.png)
![Screenshot](/Resources/screenshot2.png)
![Screenshot](/Resources/screenshot3.png)

### :hammer: Tools

- Unity
- Aseprite
- Audacity

### :bow: Thanks!

This is my first Ludum Dare game, I hope you enjoy it!
