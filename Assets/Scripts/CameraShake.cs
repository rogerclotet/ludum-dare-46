﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shakeDuration;

    private float shakeUntil;

    void Update()
    {
        if (Time.realtimeSinceStartup < shakeUntil && Mathf.CeilToInt(Time.realtimeSinceStartup * 100) % 5 == 0)
        {
            transform.position = new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(-0.2f, 0.2f), -10);
        }
        else
        {
            transform.position = new Vector3(0, 0, -10);
        }
    }

    public void Shake()
    {
        shakeUntil = Time.realtimeSinceStartup + shakeDuration;
    }
}
