using UnityEngine;

public class TakesDamage : MonoBehaviour
{
    public float startingHitPoints;

    public float HitPoints { get; set; }

    protected void Init()
    {
        HitPoints = startingHitPoints;
    }

    public void TakeDamage(float damage)
    {
        HitPoints -= damage;
        ShowDamage(damage);
        if (HitPoints <= 0)
        {
            OnDie();
        }
    }

    void ShowDamage(float damage)
    {
        FloatingTextController.CreateFloatingText(
            Mathf.RoundToInt(damage).ToString(),
            new Vector2(transform.position.x, transform.position.y + transform.localScale.y),
            "#fbf236" // Yellow
        );
    }

    public virtual void OnDie()
    {
    }
}
