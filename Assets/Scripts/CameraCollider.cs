﻿using UnityEngine;

public class CameraCollider : MonoBehaviour
{
    void Start()
    {
        Camera camera = GetComponent<Camera>();

        float aspect = (float)Screen.width / Screen.height;

        float width = 2.0f * camera.orthographicSize * aspect;
        float height = 2.0f * camera.orthographicSize;

        BoxCollider2D c1 = gameObject.AddComponent<BoxCollider2D>();
        c1.offset = new Vector2(-width / 2 - 0.5f, 0);
        c1.size = new Vector2(1, height);

        BoxCollider2D c2 = gameObject.AddComponent<BoxCollider2D>();
        c2.offset = new Vector2(0, -height / 2 - 0.5f);
        c2.size = new Vector2(width, 1);

        BoxCollider2D c3 = gameObject.AddComponent<BoxCollider2D>();
        c3.offset = new Vector2(width / 2 + 0.5f, 0);
        c3.size = new Vector2(1, height);

        BoxCollider2D c4 = gameObject.AddComponent<BoxCollider2D>();
        c4.offset = new Vector2(0, height / 2 + 0.5f);
        c4.size = new Vector2(width, 1);
    }
}
