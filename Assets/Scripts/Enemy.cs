﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum EnemyState
{
    Alive,
    Dead,
}

public class Enemy : TakesDamage
{
    public float fireCooldown;
    public int shotsFired;
    public float spread;
    public float range;
    public float moveSpeed;
    public float turnSpeed;
    public int scoreValue;
    public Bullet bulletPrefab;

    private EnemyState state;
    private Vector2 target = new Vector2(0, 0);
    private Rigidbody2D rb;
    private AudioSource audioSource;
    private float lastFired;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();

        base.Init();

        state = EnemyState.Alive;
    }

    void FixedUpdate()
    {
        if (state == EnemyState.Dead)
        {
            return;
        }

        float distance = Vector2.Distance(target, transform.position);

        if (distance > range)
        {
            FaceTarget();
            MoveToTarget();
        }
        else
        {
            FaceTarget();
            TryToFire();
        }
    }

    void FaceTarget()
    {
        Vector3 relativePos = target - (Vector2)transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, relativePos);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.fixedDeltaTime * turnSpeed);
    }

    void MoveToTarget()
    {
        Vector2 toMove = target - (Vector2)transform.position;
        float distance = toMove.magnitude;

        if (distance > range)
        {
            rb.AddForce(transform.up * moveSpeed);
        }
    }

    void TryToFire()
    {
        if (Time.realtimeSinceStartup > lastFired + fireCooldown)
        {
            float totalSpread = spread * (shotsFired - 1);
            float baseAngle = -totalSpread / 2;

            Vector3 relativePos = target - (Vector2)transform.position;
            Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, relativePos);

            if (Mathf.Abs(targetRotation.z - transform.rotation.z) > 0.1f)
            {
                return;
            }

            for (int i = 0; i < shotsFired; i++)
            {
                float angle = baseAngle + spread * i + Random.Range(-2f, 2f);
                Bullet bullet = Instantiate(bulletPrefab, transform.position, transform.rotation);
                bullet.SetTargetTags(new string[] { "Player", "Planet" });
                bullet.transform.Rotate(0, 0, angle);
            }

            lastFired = Time.realtimeSinceStartup;
        }
    }

    public override void OnDie()
    {
        state = EnemyState.Dead;

        Collider2D collider = gameObject.GetComponent<Collider2D>();
        collider.enabled = false;

        GameObject gameManager = GameObject.Find("GameManager");
        gameManager.SendMessage("AddEnemyDefeated", scoreValue);

        FloatingTextController.CreateFloatingText(
           "+" + scoreValue.ToString(),
           new Vector2(transform.position.x, transform.position.y + transform.localScale.y),
           "#ffffff"
       );

        audioSource.pitch = 0.8f;
        audioSource.Play();

        Animator animator = gameObject.GetComponent<Animator>();
        animator.Play("EnemyExplosion");
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }

    public void TakeForce(Vector2 force)
    {
        audioSource.PlayOneShot(audioSource.clip);
        rb.AddForce(force, ForceMode2D.Impulse);
    }
}
