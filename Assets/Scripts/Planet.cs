﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : TakesDamage
{
    public GameManager gameManager;
    public CameraShake cameraShake;

    private Animator animator;
    private AudioSource audioSource;

    void Start()
    {
        base.Init();

        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    public override void OnDie()
    {
        Time.timeScale = 0.5f;
        audioSource.pitch = 0.3f;
        audioSource.PlayOneShot(audioSource.clip);
        animator.Play("PlanetExplosion");
    }

    public void OnDieAnimationEnded()
    {
        gameManager.EndGame();
    }

    public void TakeForce(Vector2 force)
    {
        // No knockback effect

        audioSource.PlayOneShot(audioSource.clip);
        cameraShake.Shake();
    }
}
