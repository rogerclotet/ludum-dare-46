﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float minDamage;
    public float maxDamage;
    public float initialForce;

    private Rigidbody2D rb;
    private bool fired = false;
    private string[] targetTags;
    private float toBeDestroyedOn;

    public void SetTargetTags(string[] tags)
    {
        targetTags = tags;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        GetComponent<AudioSource>().pitch += Random.Range(0, 0.2f);
    }

    void FixedUpdate()
    {
        if (!fired)
        {
            rb.AddForce(transform.up * initialForce, ForceMode2D.Impulse);
            fired = true;
        }

        if (toBeDestroyedOn != 0 && Time.realtimeSinceStartup > toBeDestroyedOn)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (targetTags.Contains(collider.gameObject.tag))
        {
            float damage = Random.Range(minDamage, maxDamage);
            Vector2 knockBack = rb.velocity.normalized * rb.mass;
            collider.gameObject.SendMessage("TakeDamage", damage);
            collider.gameObject.SendMessage("TakeForce", knockBack);

            SetToDestroy();
        }
    }

    void OnBecameInvisible()
    {
        SetToDestroy();
    }

    void SetToDestroy()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;

        // Wait for audio to stop playing
        toBeDestroyedOn = Time.realtimeSinceStartup + 0.5f;
    }
}
