﻿using UnityEngine;
using System.Collections;

public class FloatingTextController : MonoBehaviour
{
    public static FloatingText floatingTextPrefab;

    public static void Init()
    {
        if (!floatingTextPrefab)
        {
            floatingTextPrefab = Resources.Load<FloatingText>("Prefabs/FloatingText");
        }
    }

    public static void CreateFloatingText(string text, Vector2 position, string color)
    {
        FloatingText floatingText = Instantiate(floatingTextPrefab);

        Vector2 randomizedPosition = new Vector2(position.x + Random.Range(-.2f, .2f), position.y + Random.Range(-.2f, .2f));

        floatingText.transform.position = randomizedPosition;
        floatingText.SetText(text, color);
    }
}
