﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetHealthBar : MonoBehaviour
{
    public Image healthBar;
    public Text text;
    public Planet planet;

    void Start()
    {
    }

    void Update()
    {
        float percentHitPoints = planet.HitPoints / planet.startingHitPoints;

        healthBar.fillAmount = percentHitPoints;
        text.text = Mathf.CeilToInt(Mathf.Max(0, planet.HitPoints)) + " / " + Mathf.CeilToInt(planet.startingHitPoints);
    }
}
