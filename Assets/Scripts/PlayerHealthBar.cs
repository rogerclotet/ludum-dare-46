﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    public Image healthBar;
    public Text text;
    public GameManager gameManager;

    void Start()
    {
    }

    void Update()
    {
        Player player = gameManager.Player;

        float percentHitPoints = player.HitPoints / player.startingHitPoints;

        healthBar.fillAmount = percentHitPoints;
        text.text = Mathf.CeilToInt(Mathf.Max(0, player.HitPoints)) + " / " + Mathf.CeilToInt(player.startingHitPoints);
    }
}
