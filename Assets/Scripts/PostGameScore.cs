﻿using UnityEngine;
using UnityEngine.UI;

public class PostGameScore : MonoBehaviour
{
    void Start()
    {
        int score = PlayerPrefs.GetInt("score");
        int enemiesDefeated = PlayerPrefs.GetInt("enemiesDefeated");

        GetComponent<Text>().text = GetText(score, enemiesDefeated);
    }

    string GetText(int score, int enemiesDefeated)
    {
        return "Your score was <color=#FFF>" + score + "</color>!\nYou defeated <color=#D33>" + enemiesDefeated + "</color> enemies";
    }
}
