﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    private TextMesh damageText;

    void OnEnable()
    {
        Animator animator = GetComponent<Animator>();

        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        Destroy(gameObject, clipInfo[0].clip.length);
        damageText = GetComponentInChildren<TextMesh>();
    }

    public void SetText(string text, string color)
    {
        damageText.text = "<color=" + color + ">" + text + "</color>";
    }
}
