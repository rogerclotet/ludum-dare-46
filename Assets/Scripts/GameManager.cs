﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

enum GameState
{
    Playing,
    Ended,
    Paused,
}

public class GameManager : MonoBehaviour
{
    public Player playerPrefab;
    public Planet planet;
    public List<Enemy> enemyPrefabs;
    public List<PowerUp> powerUpPrefabs;
    public float minSpawnCooldown;
    public float maxSpawnCooldown;
    public float enemySpawnRateMultiplier;
    public float enemySpawnRateCooldown;
    public float minSpawnDistance;
    public float minPowerUpCooldown;
    public float maxPowerUpCooldown;
    public GameObject pauseScreen;

    private static GameManager instance;
    private GameState state;
    private float currentMinSpawnCooldown;
    private float currentMaxSpawnCooldown;
    private float lastSpawnedEnemyTime;
    private float lastRateIncrease;
    private float lastPowerUpTime;

    public Player Player { get; protected set; }
    public int Score { get; protected set; }
    public int EnemiesDefeated { get; protected set; }

    void Awake()
    {
        instance = this;
    }

    static void Init()
    {
        instance.Score = 0;
        instance.EnemiesDefeated = 0;
        instance.lastSpawnedEnemyTime = Time.realtimeSinceStartup;
        instance.lastPowerUpTime = Time.realtimeSinceStartup;

        instance.lastRateIncrease = Time.realtimeSinceStartup;
        instance.currentMinSpawnCooldown = instance.minSpawnCooldown;
        instance.currentMaxSpawnCooldown = instance.maxSpawnCooldown;

        if (instance.Player)
        {
            Destroy(instance.Player);
        }
        instance.Player = Instantiate(instance.playerPrefab, new Vector2(0, 3), Quaternion.identity);

        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(enemy);
        }

        instance.planet.HitPoints = instance.planet.startingHitPoints;

        instance.SpawnEnemy();

        instance.state = GameState.Playing;

        Time.timeScale = 1;
    }

    void Start()
    {
        Init();

        FloatingTextController.Init();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (state == GameState.Playing)
            {
                Pause();
                return;
            }

            if (state == GameState.Paused)
            {
                Resume();
                return;
            }
        }

        if (state != GameState.Playing)
        {
            return;
        }

        if (planet.HitPoints <= 0)
        {
            PlayerPrefs.SetInt("score", Score);
            PlayerPrefs.SetInt("enemiesDefeated", EnemiesDefeated);

            state = GameState.Ended;

            return;
        }

        HandleEnemyRateIncrease();
        HandleEnemySpawns();
        HandlePowerUpSpawns();

        Player.HandleMouseInput();
    }

    void FixedUpdate()
    {
        if (state != GameState.Playing)
        {
            return;
        }

        Player.HandleAxisInput();
    }

    void HandleEnemyRateIncrease()
    {
        if (Time.realtimeSinceStartup > lastRateIncrease + enemySpawnRateCooldown)
        {
            currentMinSpawnCooldown *= enemySpawnRateMultiplier;
            currentMaxSpawnCooldown *= enemySpawnRateMultiplier;

            lastRateIncrease = Time.realtimeSinceStartup;
        }
    }

    void HandleEnemySpawns()
    {
        if (Time.realtimeSinceStartup > lastSpawnedEnemyTime + currentMinSpawnCooldown)
        {
            float spawnCooldown = Random.Range(currentMinSpawnCooldown, currentMaxSpawnCooldown);

            if (Time.realtimeSinceStartup > lastSpawnedEnemyTime + spawnCooldown)
            {
                SpawnEnemy();
                lastSpawnedEnemyTime = Time.realtimeSinceStartup;
            }
        }
    }

    void SpawnEnemy()
    {
        Vector2 bounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        Vector2 pos;
        do
        {
            pos = new Vector2(Random.Range(-bounds.x, bounds.x), Random.Range(-bounds.y, bounds.y));
        } while (Vector2.Distance(pos, planet.transform.position) < minSpawnDistance);

        Vector2 relativePos = (Vector2)planet.transform.position - pos;

        int randomIndex = Random.Range(0, enemyPrefabs.Count);
        Enemy enemyPrefab = enemyPrefabs[randomIndex];

        Quaternion rotation = Quaternion.LookRotation(Vector3.forward, relativePos);
        Enemy enemy = Instantiate(enemyPrefab, pos, rotation);
        enemy.transform.Rotate(0, 0, Random.Range(-30f, 30f));
    }

    public void AddEnemyDefeated(int points)
    {
        EnemiesDefeated++;
        Score += points;
    }

    void HandlePowerUpSpawns()
    {
        if (Time.realtimeSinceStartup > lastPowerUpTime + minPowerUpCooldown)
        {
            float spawnCooldown = Random.Range(minPowerUpCooldown, maxPowerUpCooldown);

            if (Time.realtimeSinceStartup > lastPowerUpTime + spawnCooldown)
            {
                SpawnPowerUp();
                lastPowerUpTime = Time.realtimeSinceStartup;
            }
        }
    }

    void SpawnPowerUp()
    {
        int randomIndex = Random.Range(0, powerUpPrefabs.Count);
        PowerUp powerUpPrefab = powerUpPrefabs[randomIndex];

        PowerUp powerUp = Instantiate(powerUpPrefab, planet.transform.position, Quaternion.identity);
        Rigidbody2D rb = powerUp.GetComponent<Rigidbody2D>();

        // TODO use better random for this
        Vector2 direction = new Vector2(
            Random.Range(-1, 1),
            Random.Range(-1, 1)
        );
        direction.Normalize();
        powerUp.transform.position += planet.GetComponent<CircleCollider2D>().radius * (Vector3)direction;
        rb.AddForce(direction * 5, ForceMode2D.Impulse);
    }

    public void EndGame()
    {
        SceneManager.LoadSceneAsync("PostGame");

        Time.timeScale = 0;
    }

    public void Pause()
    {
        Time.timeScale = 0;
        state = GameState.Paused;

        pauseScreen.SetActive(true);
    }

    public void Resume()
    {
        pauseScreen.SetActive(false);

        Time.timeScale = 1;
        state = GameState.Playing;
    }
}
