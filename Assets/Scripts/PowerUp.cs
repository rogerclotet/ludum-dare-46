﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public float fireCooldownReduction;
    public int extraShots;
    public float expiration;

    private float spawnTime;
    private float toBeDestroyedAt;

    void Start()
    {
        spawnTime = Time.realtimeSinceStartup;
    }

    void Update()
    {
        if (toBeDestroyedAt != 0)
        {
            if (Time.realtimeSinceStartup >= toBeDestroyedAt)
            {
                SelfDestruct();
            }

            return;
        }

        if (Time.realtimeSinceStartup > spawnTime + expiration)
        {
            Animator animator = GetComponent<Animator>();
            animator.Play("Flicker");
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag != "Player")
        {
            return;
        }

        if (fireCooldownReduction > 0)
        {
            collider.gameObject.SendMessage("ApplyFireCooldownReduction", fireCooldownReduction);

            FloatingTextController.CreateFloatingText(
                "Fire rate up!",
                new Vector2(transform.position.x, transform.position.y + transform.localScale.y),
                "#6abe30" // Green
            );
        }

        if (extraShots > 0)
        {
            collider.gameObject.SendMessage("ApplyMultiShot", extraShots);

            FloatingTextController.CreateFloatingText(
               "Extra shot!",
               new Vector2(transform.position.x, transform.position.y + transform.localScale.y),
               "#639bff" // Blue
           );
        }

        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.Play();

        GetComponent<Collider2D>().enabled = false;
        GetComponentInChildren<SpriteRenderer>().enabled = false;

        toBeDestroyedAt = Time.realtimeSinceStartup + 0.5f;
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
