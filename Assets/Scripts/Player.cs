﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : TakesDamage
{
    public Bullet bulletPrefab;
    public float throttleForce;
    public int shotsFired;
    public float fireCooldown;
    public float spread;

    private Rigidbody2D rb;
    private Animator animator;
    private AudioSource audioSource;
    private float lastFired;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

        base.Init();
    }

    public void HandleMouseInput()
    {
        FaceMouse();

        if (Input.GetMouseButton(0) && Time.realtimeSinceStartup > lastFired + fireCooldown)
        {
            Fire();
            lastFired = Time.realtimeSinceStartup;
        }
    }

    public void HandleAxisInput()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector2 force = new Vector2(horizontal, vertical);

        rb.AddForce(force * throttleForce * Time.fixedDeltaTime);

        animator.SetFloat("Throttle", force.magnitude);
    }

    public override void OnDie()
    {
        audioSource.pitch = 0.5f;
        audioSource.Play();

        animator.Play("PlayerExplosion");
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }

    void FaceMouse()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 facingDirection = mousePos - transform.position;

        transform.up = new Vector2(facingDirection.x, facingDirection.y);
    }

    void Fire()
    {
        float totalSpread = spread * (shotsFired - 1);
        float baseAngle = -totalSpread / 2;

        for (int i = 0; i < shotsFired; i++)
        {
            float angle = baseAngle + spread * i + Random.Range(-2f, 2f);
            Bullet bullet = Instantiate(bulletPrefab, transform.position, transform.rotation);
            bullet.SetTargetTags(new string[] { "Enemy" });
            bullet.transform.Rotate(0, 0, angle);
        }
    }

    public void ApplyFireCooldownReduction(float fireCooldownPercent)
    {
        fireCooldown = fireCooldown / (1 + fireCooldownPercent);
    }

    public void TakeForce(Vector2 force)
    {
        audioSource.PlayOneShot(audioSource.clip);
        rb.AddForce(force, ForceMode2D.Impulse);
    }

    public void ApplyMultiShot(int extraShots)
    {
        shotsFired += extraShots;
    }
}
