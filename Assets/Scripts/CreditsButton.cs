﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsButton : MonoBehaviour
{
    public void ShowCredits()
    {
        SceneManager.LoadScene("Credits");
    }
}
